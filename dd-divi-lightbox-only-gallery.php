<?php 
/**
 * Plugin Name: DD Divi Lightbox Only Gallery
 * Plugin URI: https://bitbucket.org/d2roth/dd-divi-lightbox-only-gallery
 * Description: This plugin allows Divi galleries to be only a lightbox and triggered from a button instead of showing the grid. Basic use: Add an ID to the gallery module and create a button with the href set to #gallery-id where gallery-id is the ID you added to the gallery module. Then add the CSS Class <code>dlo-gallery-trigger</code> to the button. (optional) If you don't want the gallery to be visible as a grid you can add <code>display:none!important;</code> to the Advaced CSS > Main Element section of the gallery.
 * Version:     0.1.0
 * Author:      Daniel Roth
 * License:     GPL
 */

if(!class_exists('DD_Divi_Lightbox_Gallery')){
	
	class DD_Divi_Lightbox_Gallery{
		
		public function __construct(){
			add_action( 'wp_head', [$this, 'add_head_css'], 9999 );
			add_action( 'wp_footer', [$this, 'add_footer_scripts'], 9999 );
		}
		
		/**
		 * Adds the required jQuery to the footer
		 *
		 * @since  0.1.0
		 *
		 * @return void
		 */
		public function add_footer_scripts() {
			?>
			<script type="text/javascript">
			jQuery('.dlo-gallery-trigger').on('click',function(e){
				var target = jQuery(this).attr('href');
				jQuery(target + ' a').first().click();
			});
			</script>
			<?php
		}

	}

}

new DD_Divi_Lightbox_Gallery();
